# -*- coding: utf-8 -*-
import datetime
import uuid
import pytz
from decimal import Decimal as D
from django.conf import settings
from django.db import models
from django.db.models import Avg
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from timezone_field import TimeZoneField

from oscar.core.compat import AUTH_USER_MODEL
from zs_core.codes import CANCELLED, STATUS_CHOICES


class UnavailableSchedule(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL,
                             related_name='unavailable_scheds',
                             verbose_name=_('Workers'),
                             on_delete=models.CASCADE)
    title = models.CharField(_('title'), max_length=256)
    start = models.DateTimeField()
    end = models.DateTimeField()
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    timezone = TimeZoneField(null=True, blank=True)

    class Meta:
        verbose_name = _('Unavailable Schedule')
        ordering = ['-start']

    def user_owns_schedule(self, user):
        return self.user == user

    def __unicode__(self):
        return u"%s [%s-%s]" % (self.title, self.start.strftime("%c"), self.end.strftime("%c"))


class Color(models.Model):
    code = models.CharField(_('code'), max_length=7, unique=True,
        help_text=_("Hexadecimal color code with format: '#XXXXXX'"))
    user = models.OneToOneField(AUTH_USER_MODEL, related_name='color',
        blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return "{} ({})".format(self.code, getattr(self, 'user', None))


class Appointment(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    start = models.DateTimeField()
    end = models.DateTimeField()
    padded_start = models.DateTimeField()
    padded_end = models.DateTimeField()
    order = models.ForeignKey('order.Order', on_delete=models.CASCADE)
    worker = models.ForeignKey(AUTH_USER_MODEL, related_name='appointments',
                               verbose_name=_('Worker'),
                               on_delete=models.CASCADE)
    status = models.CharField(
        choices=STATUS_CHOICES, default=STATUS_CHOICES[0][0],
        max_length=20, verbose_name=_("Status"))
    cancellation_datetime = models.DateTimeField(blank=True, null=True)
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    pickup_address = models.ForeignKey('address.UserAddress', null=True,
                                       blank=True, on_delete=models.SET_NULL,
                                       related_name='pickup_appointments')
    shipping_address = models.ForeignKey('address.UserAddress', null=True,
                                         blank=True, on_delete=models.SET_NULL,
                                         related_name='shipping_appointments')
    timezone = TimeZoneField(null=True, blank=True)
    # TODO: must track multiple devices as well..
    four_hours_prior_notification_sent = models.DateTimeField(null=True,
                                                              blank=True)
    one_hour_prior_notification_sent = models.DateTimeField(null=True,
                                                            blank=True)
    started_notification_sent = models.DateTimeField(null=True, blank=True)
    rating_notification_sent = models.DateTimeField(null=True, blank=True)
    day_prior_email_sent = models.DateTimeField(null=True, blank=True)
    day_prior_notification_sent = models.DateTimeField(null=True, blank=True)

    pipeline = getattr(settings, 'APPOINTMENT_STATUS_PIPELINE', {})

    objects = models.Manager()

    def available_statuses(self):
        """
        Return all possible statuses that this appointment can move to
        """
        return self.pipeline.get(self.status, ())

    def is_refundable(self):
        # appointment is only refundable if cancelled up to 6 hours before the
        # booking appointment
        now = datetime.datetime.now(pytz.utc)
        start = self.start
        if (start - now).total_seconds()/3600 < 6:
            return False
        else:
            return True

    def get_date_created(self):
        return self.order.date_placed

    def __str__(self):
        return "%s [%s-%s]" % (self.look.title, self.start.strftime("%c"), self.end.strftime("%c"))

WEEKDAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
            'Saturday', 'Sunday']


class WorkSchedule(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL, related_name='work_schedules',
                                verbose_name=_('User'), on_delete=models.CASCADE)
    # store as integer since weekday() returns day of the week as an integer,
    # where Monday is 0 and Sunday is 6.
    weekday = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(6)])
    start = models.TimeField()
    end = models.TimeField()
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)

    class Meta:
        ordering = ['weekday', 'start']

    def __unicode__(self):
        return "{}: {} {}-{}".format(self.user.get_full_name(),
                                     self.weekday_name, self.start, self.end)

    @property
    def weekday_name(self):
        return(WEEKDAYS[self.weekday])


class AppointmentPadding(SingletonModel):
    """
    use this for global padding setting
    """
    minutes = models.PositiveIntegerField(
        _('Minutes'), help_text=_("padding before and after appointment in minutes"), default=15)


class CurrentDateTimePadding(SingletonModel):
    minutes = models.PositiveIntegerField(
        _("Minutes"), help_text=_("padding in minutes from current datetime "
        "for booking appointments"), default=60)


class AvailableSchedulesInterval(SingletonModel):
    minutes = models.PositiveIntegerField(_("Minutes"), help_text=_(
        "interval in minutes for start times in available schedules"),
        default=15, validators=[MinValueValidator(1)])
