.. :changelog:

History
-------

0.2.0 (2018-08-01)
++++++++++++++++++

* First release on PyPI.
