#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def get_version(*file_paths):
    """Retrieves the version from oscar_appointments/__init__.py"""
    filename = os.path.join(os.path.dirname(__file__), *file_paths)
    version_file = open(filename).read()
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError('Unable to find version string.')


version = get_version("oscar_appointments", "__init__.py")


if sys.argv[-1] == 'publish':
    try:
        import wheel
        print("Wheel version: ", wheel.__version__)
    except ImportError:
        print('Wheel library missing. Please run "pip install wheel"')
        sys.exit()
    os.system('python setup.py sdist upload')
    os.system('python setup.py bdist_wheel upload')
    sys.exit()

if sys.argv[-1] == 'tag':
    print("Tagging the version on git:")
    os.system("git tag -a %s -m 'version %s'" % (version, version))
    os.system("git push --tags")
    sys.exit()

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='dj-oscar-appointments',
    version=version,
    description="""Your project description goes here""",
    long_description=readme + '\n\n' + history,
    author='Aida Delos Reyes',
    author_email='acsdelosreyes@softwarelab7.com',
    url='https://github.com/yourname/dj-oscar-appointments',
    packages=[
        'oscar_appointments',
    ],
    include_package_data=True,
    install_requires=[
        'django==2.0.7',
        'django-braces==1.13.0',
        'django-solo==1.1.3',
        'django-oscar==1.6.2',
        'django-pipeline==1.6.14',
        'django-timezone-field==2.1',
        'geopy==1.16.0',
        'pygeocoder==1.2.5'
    ],
    license="MIT",
    zip_safe=False,
    keywords='dj-oscar-appointments',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Framework :: Django :: 2.0',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
)
