import datetime
import pytz

from django import forms
from django.utils.timezone import get_current_timezone
from django.utils.translation import ugettext_lazy as _

from oscar.core.compat import get_user_model
from oscar.forms import widgets

from .models import AppointmentPadding, WorkSchedule
from dj_address_utils.utils import get_address_timezone
from zs_core.fields import UserModelChoiceField
from zs_core.utils import get_form_tzinfo


User = get_user_model()


class AddUnavailableScheduleForm(forms.Form):
    title = forms.CharField(max_length=256)
    start_date = forms.DateField(required=True, widget=widgets.DatePickerInput)
    start_time = forms.TimeField(
        required=True,
        widget=widgets.TimePickerInput(format='%H:%M'),
        label=_('Start Time')
    )
    end_date = forms.DateField(required=True, widget=widgets.DatePickerInput)
    end_time = forms.TimeField(
        required=True,
        widget=widgets.TimePickerInput(format='%H:%M'),
        label=_('End Time')
    )

    def __init__(self, *args, **kwargs):
        self.address = kwargs.pop('address', None)
        super(AddUnavailableScheduleForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(AddUnavailableScheduleForm, self).clean()
        # require address, so unavailable schedule is always in the
        # right timezone
        if not self.address:
            raise forms.ValidationError(_("There is no set address."))
        # !! clean still gets called even if there are errors in other fields
        # still need to check that these all exist
        start_date = cleaned_data.get('start_date', None)
        start_time = cleaned_data.get('start_time', None)
        end_date = cleaned_data.get('end_date', None)
        end_time = cleaned_data.get('end_time', None)
        if (start_date is None or start_time is None or end_date is None or end_time is None):
            return cleaned_data
        start = datetime.datetime.combine(start_date, start_time)
        # use tzinfo from user partner address
        timezone = get_address_timezone(self.address)
        if not timezone:
            raise forms.ValidationError(_("Unable to get the timezone for"
                "the default address. Please try again later."))
        start = timezone.localize(start)
        end = datetime.datetime.combine(end_date, end_time)
        end = timezone.localize(end)
        if (end <= start):
            raise forms.ValidationError(_("The end date and time of the "
                "unavailable schedule must be after the start date and time"))
        else:
            cleaned_data['start'] = start
            cleaned_data['end'] = end
        return cleaned_data


class AppointmentPaddingForm(forms.ModelForm):
    model = AppointmentPadding


class TimezoneForm(forms.Form):
    timezone = forms.ChoiceField(choices=[(x, x) for x in pytz.common_timezones],
                                 widget=forms.Select(attrs={'class': 'no-widget-init'}))

    def __init__(self, *args, **kwargs):
        timezone = kwargs.pop('timezone', None)
        super(TimezoneForm, self).__init__(*args, **kwargs)
        if timezone:
            self.fields['timezone'].initial = timezone
        else:
            self.fields['timezone'].initial = get_current_timezone()


class WorkScheduleAdminForm(forms.Form):
    user = UserModelChoiceField(
        queryset=User.objects.none(),
        widget=forms.Select(attrs={'class': 'no-widget-init'}))

    def __init__(self, request, *args, **kwargs):
        super(WorkScheduleAdminForm, self).__init__(*args, **kwargs)
        active_workers = User.active_workers.all()
        user_pks = WorkSchedule.objects.filter(
            user__in=active_workers).values_list('user', flat=True)
        user_set = User.objects.filter(pk__in=user_pks)
        self.fields['user'].queryset = user_set.exclude(
            id=request.user.id)
        if 'user_id' in request.session:
            self.fields['user'].initial = request.session['user_id']


class WorkScheduleForm(forms.ModelForm):
    start = forms.TimeField(
        required=True,
        widget=widgets.TimePickerInput(format='%H:%M'),
        label=_('Start')
    )
    end = forms.TimeField(
        required=True,
        widget=widgets.TimePickerInput(format='%H:%M'),
        label=_('End')
    )
    weekday = forms.ChoiceField(choices=[
        (0, 'Monday'), (1, 'Tuesday'), (2, 'Wednesday'), (3, 'Thursday'),
        (4, 'Friday'), (5, 'Saturday'), (6, 'Sunday')])

    class Meta:
        model = WorkSchedule
        widgets = {'user': forms.HiddenInput()}
        fields = ('start', 'end', 'weekday', 'user')


class WorkScheduleCreateForm(forms.Form):
    start = forms.TimeField(
        required=True,
        widget=widgets.TimePickerInput(format='%H:%M'),
        label=_('Start')
    )
    end = forms.TimeField(
        required=True,
        widget=widgets.TimePickerInput(format='%H:%M'),
        label=_('End')
    )
    weekdays = forms.MultipleChoiceField(choices=[
        (0, 'Monday'), (1, 'Tuesday'), (2, 'Wednesday'), (3, 'Thursday'),
        (4, 'Friday'), (5, 'Saturday'), (6, 'Sunday')])

    def __init__(self, instance, *args, **kwargs):
        super(WorkScheduleCreateForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        start = cleaned_data.get('start', None)
        end = cleaned_data.get('end', None)
        if start is None or end is None:
            return cleaned_data
        if (end <= start):
            raise forms.ValidationError(_("The end time must be after the start time"))
        return cleaned_data


class NewWorkScheduleCreateForm(WorkScheduleCreateForm):
    user = UserModelChoiceField(
        queryset=User.objects.none(),
        widget=forms.Select(attrs={'class': 'no-widget-init'}))

    def __init__(self, *args, **kwargs):
        super(NewWorkScheduleCreateForm, self).__init__(*args, **kwargs)
        self.fields['user'].queryset = User.active_workers.all()

    def clean(self):
        cleaned_data = self.cleaned_data
        start = cleaned_data.get('start', None)
        end = cleaned_data.get('end', None)
        if start is None or end is None:
            return cleaned_data
        if (end <= start):
            raise forms.ValidationError(_("The end time must be after the start time"))
        return cleaned_data
