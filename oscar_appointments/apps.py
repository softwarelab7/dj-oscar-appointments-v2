# -*- coding: utf-8
from django.apps import AppConfig


class OscarAppointmentsConfig(AppConfig):
    name = 'oscar_appointments'
