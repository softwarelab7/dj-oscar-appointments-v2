=============================
Django Oscar Appointments
=============================

.. image:: https://badge.fury.io/py/dj-oscar-appointments.svg
    :target: https://badge.fury.io/py/dj-oscar-appointments

.. image:: https://travis-ci.org/yourname/dj-oscar-appointments.svg?branch=master
    :target: https://travis-ci.org/yourname/dj-oscar-appointments

.. image:: https://codecov.io/gh/yourname/dj-oscar-appointments/branch/master/graph/badge.svg
    :target: https://codecov.io/gh/yourname/dj-oscar-appointments

Your project description goes here

Documentation
-------------

The full documentation is at https://dj-oscar-appointments.readthedocs.io.

Quickstart
----------

Install Django Oscar Appointments::

    pip install dj-oscar-appointments

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'oscar_appointments.apps.OscarAppointmentsConfig',
        ...
    )

Add Django Oscar Appointments's URL patterns:

.. code-block:: python

    from oscar_appointments import urls as oscar_appointments_urls


    urlpatterns = [
        ...
        url(r'^', include(oscar_appointments_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox

Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
