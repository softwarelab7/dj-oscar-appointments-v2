============
Installation
============

At the command line::

    $ easy_install dj-oscar-appointments

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv dj-oscar-appointments
    $ pip install dj-oscar-appointments
