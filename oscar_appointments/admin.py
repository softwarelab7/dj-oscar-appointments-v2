from django.contrib import admin
from .models import Appointment, Color, WorkSchedule, UnavailableSchedule


class AppointmentAdmin(admin.ModelAdmin):
    search_fields = ['order__number']
    list_filter = ['status']


class WorkScheduleAdmin(admin.ModelAdmin):
    search_fields = ['user__email', 'user__first_name', 'user__last_name', 'user__username']


class UnavailableScheduleAdmin(admin.ModelAdmin):
    search_fields = ['user__email', 'user__first_name', 'user__last_name', 'user__username']


class ColorAdmin(admin.ModelAdmin):
    search_fields = ['user__email', 'user__first_name', 'user__last_name', 'user__username']


admin.site.register(Appointment, AppointmentAdmin)
admin.site.register(Color, ColorAdmin)
admin.site.register(WorkSchedule, WorkScheduleAdmin)
admin.site.register(UnavailableSchedule, UnavailableScheduleAdmin)
