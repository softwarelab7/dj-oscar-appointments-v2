=====
Usage
=====

To use Django Oscar Appointments in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'oscar_appointments.apps.OscarAppointmentsConfig',
        ...
    )

Add Django Oscar Appointments's URL patterns:

.. code-block:: python

    from oscar_appointments import urls as oscar_appointments_urls


    urlpatterns = [
        ...
        url(r'^', include(oscar_appointments_urls)),
        ...
    ]
