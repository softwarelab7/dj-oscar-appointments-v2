from django.urls import path

from oscar.core.application import Application

from . import views


class ScheduleDashboardApplication(Application):
    name = 'schedules'

    # Stylists have to be Partners. Staff can only see Admin Schedule
    permissions_map = _map = {
        'create': ['partner.dashboard_access'],
        'detail': (['is_staff'], ['partner.dashboard_access']),
        'edit': ['partner.dashboard_access'],
        'delete': ['partner.dashboard_access'],
        'appointment_padding': ['is_staff'],
        'appontment_detail': (['is_staff'], ['partner.dashboard_access']),
        'available-schedules-interval': ['is_staff'],
        'admin-work-schedules': ['is_staff'],
        'work-schedules': ['partner.dashboard_access'],
        'work-schedule-create': ['partner.dashboard_access'],
        'admin-work-schedule-create': ['is_staff'],
        'new-admin-work-schedule-create': ['is_staff'],
        'admin-per-user-work-schedules': ['is_staff'],
        'admin-work-schedule-delete': (['is_staff'], ['partner.dashboard_access']),
        'admin-work-schedule-update': (['is_staff'], ['partner.dashboard_access']),
        'now_padding': ['is_staff'],
        'color-list': ['is_staff'],
        'color-create': ['is_staff'],
        'color-edit': ['is_staff'],
    }

    list_view = views.UnavailableScheduleListView
    create_view = views.UnavailableScheduleFormView
    update_view = views.UnavailableScheduleUpdateView
    delete_view = views.UnavailableScheduleDeleteView
    appointment_padding_view = views.AppointmentPaddingView
    appointment_detail_view = views.AppointmentDetailView
    available_schedules_interval_view = views.AvailableSchedulesIntervalView
    admin_work_schedules_view = views.WorkScheduleAdminView
    admin_per_user_work_schedules_view = views.PerUserWorkScheduleAdminView
    work_schedules_view = views.WorkScheduleView
    new_work_schedules_create_view = views.NewWorkScheduleCreateView
    work_schedules_delete_view = views.WorkScheduleDeleteView
    work_schedules_update_view = views.WorkScheduleUpdateView
    work_schedules_create_view = views.WorkScheduleCreateView
    now_padding_view = views.CurrentDatetimePaddingView
    color_list_view = views.ColorListView
    color_update_view = views.ColorUpdateView
    color_create_view = views.ColorCreateView

    def get_urls(self):
        urlpatterns = [
            path('', self.list_view.as_view(), name='list'),
            path('create/', self.create_view.as_view(),
                name='create'),
            path('<int:pk>/edit/', self.update_view.as_view(),
                name='edit'),
            path('<int:pk>/delete/', self.delete_view.as_view(),
                name='delete'),
            path('appointment/padding/', self.appointment_padding_view.as_view(),
                name='appointment_padding'),
            path('appointment/now_padding/', self.now_padding_view.as_view(),
                name='now_padding'),
            path('appointment/<int:pk>/', self.appointment_detail_view.as_view(),
                name='appointment_detail'),
            path('colors/', self.color_list_view.as_view(),
                name='color-list'),
            path('colors/create/', self.color_create_view.as_view(),
                name='color-create'),
            path('colors/<int:pk>/', self.color_update_view.as_view(),
                name='color-edit'),
            path('interval/', self.available_schedules_interval_view.as_view(),
                name='available-schedules-interval'),
            path('work_schedules/', self.work_schedules_view.as_view(),
                name='work-schedules'),
            path('work_schedules/<int:pk>/create/', self.work_schedules_create_view.as_view(admin=False),
                name='work-schedule-create'),
            path('work_schedules/create/', self.new_work_schedules_create_view.as_view(),
                name='new-admin-work-schedule-create'),
            path('work_schedules/<int:pk>/', self.work_schedules_update_view.as_view(),
                name='admin-work-schedule-update'),
            path('work_schedules/<int:pk>/delete/', self.work_schedules_delete_view.as_view(),
                name='admin-work-schedule-delete'),
            path('work_schedules/admin/<int:pk>/create/', self.work_schedules_create_view.as_view(admin=True),
                name='admin-work-schedule-create'),
            path('work_schedules/admin/', self.admin_work_schedules_view.as_view(),
                name='admin-work-schedules'),
            path('work_schedules/admin/<int:pk>/', self.admin_per_user_work_schedules_view.as_view(admin=True),
                name='admin-per-user-work-schedules'),
            path('work_schedules/user/<int:pk>/', self.admin_per_user_work_schedules_view.as_view(admin=False),
                name='per-user-work-schedules'),
        ]
        return self.post_process_urls(urlpatterns)


application = ScheduleDashboardApplication()
